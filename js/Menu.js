//функция конструктор меню
function NavigationMenu(options){
	//свойства
	this._elem = options.elem;
	this._targt;
	this._previousTarget;
	this._img;
}
	//методы
	//метод создание запроса и отправки на сервер
NavigationMenu.prototype.loadLink = function(link){
	xhr = this.getXmlHttpRequest();	
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4){
			var tag = document.querySelector('.contextMobile');
			if(xhr.status != 200){
				console.log("Cтатус: " + xhr.status + " " + xhr.statusText);
			}else{				
				tag.innerHTML = xhr.responseText;
			}
			if(xhr.status == 404){
				let img;
				if(!img){
					img = document.createElement('img');
					img.src = './img/404.png';
					img.alt = '404.png';
				}	
				tag.innerHTML = '';
				tag.appendChild(img);
				console.log(img);

			}
		}
	}

	xhr.open('POST', link, true);
	xhr.send(null);
	
};
	//создание XmlHttpRequest
NavigationMenu.prototype.getXmlHttpRequest = function(){
	if(window.XMLHttpRequest){
		try{
			return new XMLHttpRequest();
		}
		catch(e){}
	}else if(window.ActiveXObject){
		try{
			return new ActiveXObject('Msxml2.XMLHTTP');

		}catch(e){}
		try{
			return new ActiveXObject('Microsoft.XMLHTTP');

		}catch (e){}
	}
	return null;
	
};
	//проверка на существование img
NavigationMenu.prototype.getImg = function(){
		if (!this._img) this.renderImg();
		return this._img;
};
	//получаем elem
NavigationMenu.prototype.getElem = function(){
	return this._elem;
};
	//создание элемена img
NavigationMenu.prototype.renderImg = function(){
		this._img = document.createElement('img');
		this._img.src = './img/navHover.png';
};
	//добавление img на страницу
NavigationMenu.prototype.addImg = function(li){
		this.getImg();
		li.appendChild(this._img);
};
	//второе событие по нажатию левой мыши
NavigationMenu.prototype.Elem_click = function(event){
	if (this._previousTarget) this._previousTarget.style.cssText = "color: #0c9bdd;";
	this._targt = event.target;
	this.addImg(this._targt);
	this._targt.style.cssText = "color: #ffffff;";
	this._previousTarget = this._targt;
	this.loadLink(this._targt.getAttribute('data-href'));
	return false;
};

//левое меню aside
function LeftMenu(options){
	this._elem = options.elem;
}
//наследование от NavigationMenu
LeftMenu.prototype = Object.create(NavigationMenu.prototype);
//сохраняем конструктор LeftMenu
LeftMenu.prototype.constructor = LeftMenu;
//переопределение метода родителя
LeftMenu.prototype.Elem_click = function(event){
	var targt = event.target;
	if (targt.tagName != 'LI') targt = targt.parentElement;
	this.loadLink(targt.getAttribute('data-href'));

};


